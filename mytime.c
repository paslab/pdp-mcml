#include <assert.h>
#include <time.h>

#include "mytime.h"

#ifndef TIME_LEN
	#define TIME_LEN 8
#endif

static struct timespec ts[TIME_LEN];

void SetTime(unsigned int id) {
    assert(id < TIME_LEN);
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts[id]);
}

double DiffTime(unsigned int id2, unsigned int id1) {
    assert(id1 < TIME_LEN);
    assert(id2 < TIME_LEN);
    return difftime(ts[id2].tv_sec, ts[id1].tv_sec) + (double) (ts[id2].tv_nsec - ts[id1].tv_nsec) * 1e-9;
}
