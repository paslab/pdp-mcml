override CPPFLAGS+=
override CFLAGS+=-std=gnu99 -O3 -flto -Wall -Wextra -fopenmp
override LDFLAGS+=-fopenmp
override LIBS+=-lm
OBJS=mcmlmain.o mcmlgo.o mcmlio.o mcmlnr.o mytime.o
RM=/bin/rm -rf

all: mcml

mcml: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

clean::
	$(RM) mcml
	$(RM) $(OBJS)
